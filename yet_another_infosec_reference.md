# Yet Another InfoSec Reference

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Scripts](#scripts)
- [Services](#services)
- [PrivEsc](#privesc)
- [Exploit Dev](#exploit-dev)
- [Red Team](#red-team)
- [Tools](#tools)
- [Firewalls](#firewalls)
- [Malware](#malware)
- [Hardening](#hardening)
- [Blue Team](#blue-team)
- [Certificate Management](#certificate-management)
- [OSINT](#osint)
- [Cloud-ish](#cloud-ish)
- [General resources](#general-resources)
- [TODO](#todo)
<!-- END doctoc generated TOC please keep comment here to allow auto update -->

### Scripts:
* Grabbing Wifi Passwords with PS
    * (netsh wlan show profiles) | Select-String "\:(.+)$" | %{$name=$_.Matches.Groups[1].Value.Trim(); $_} | %{(netsh wlan show profile name="$name" key=clear)}  | Select-String "Key Content\W+\:(.+)$" | %{$pass=$_.Matches.Groups[1].Value.Trim(); $_} | %{[PSCustomObject]@{ PROFILE_NAME=$name;PASSWORD=$pass }} | Format-Table -AutoSize
* Distributed nmap scanning
    * https://github.com/rackerlabs/scantron 

### Services
* SMB
    * https://github.com/CoreSecurity/impacket 
* Splunk:
    * https://www.exploit-db.com/exploits/44865/ 
* SSH
    * SSHv1 (Nessus): Make sure to test with the ssh client (apt-get install openssh-client-1) because both nmap and Nessus are incorrect.
* SMTP
    * DMARC
* DNS
    * https://appsecco.com/books/subdomain-enumeration/active_techniques/zone_walking.html 
* Active Directory
    * https://hausec.com/2019/03/05/penetration-testing-active-directory-part-i/ 
* Addressing
    * https://support.microsoft.com/en-us/help/164015/understanding-tcp-ip-addressing-and-subnetting-basics 
    * https://oav.net/mirrors/cidr.html 

### PrivEsc
* Windows
    * General
        * https://www.absolomb.com/2018-01-26-Windows-Privilege-Escalation-Guide/
    * DLL Hijacking
    * Network
        * https://byt3bl33d3r.github.io/practical-guide-to-ntlm-relaying-in-2017-aka-getting-a-foothold-in-under-5-minutes.html 
* Linux
    * LD_PRELOAD 
    * setuid/setguid
        * Scripts, backdoors such as setting lesser used text editors, etc.
    * Binaries that can be used for exploitation:
* Resources
    * https://github.com/sagishahar/lpeworkshop 

### Exploit Dev
* Windows
    * http://www.securitysift.com/windows-exploit-development-part-1-basics/ 
    * http://www.fuzzysecurity.com/tutorials/expDev/1.html 
* Linux
* General
    * https://github.com/FabioBaroni/awesome-exploit-development

### Red Team
* Key bitting
    * Deviant Ollam: https://youtu.be/AayXf5aRFTI?t=756
        * https://github.com/deviantollam/decoding 
        * http://www.hudsonlock.com/key-machine.html 
        * https://www.lockpicks.com/pak-a-punch-value-set-for-automotive.html 
* Lockpicks
    * https://www.lockpickworld.com/products/lishi-2-in-1-pick-decoder 
* Phishing
    * Email dump: http://www.skymem.info/ 
* Tool list
    * https://github.com/infosecn1nja/Red-Teaming-Toolkit 
    * Linux binaries
        * https://gtfobins.github.io/  
* Default Credentials
    * Tomcat: https://github.com/netbiosX/Default-Credentials/blob/master/Apache-Tomcat-Default-Passwords.mdown 
* Tech Stack
    * Nginx: https://i.blackhat.com/us-18/Wed-August-8/us-18-Orange-Tsai-Breaking-Parser-Logic-Take-Your-Path-Normalization-Off-And-Pop-0days-Out-2.pdf 
* Webshells
    * http://repository.root-me.org/Exploitation%20-%20Web/EN%20-%20Webshells%20In%20PHP,%20ASP,%20JSP,%20Perl,%20And%20ColdFusion.pdf
*https://github.com/swisskyrepo/PayloadsAllTheThings/tree/master/Upload%20insecure%20files 
* Directory Traversal
    * https://github.com/wireghoul/dotdotpwn 
* Password lists
    * https://www.routerpasswords.com/ 

### Tools
* Responder
    * https://github.com/lgandx/Responder
    * Description
        * Responder is a multifunction tool.. Blah blah
    * Setup
        * Turn off the SMB and HTTP server in the Responder.conf
        * Start Responder with -bfwF (or -rv)
        * Scan the network to find servers which do not have SMB signing enabled.
        * Run MultiRelay targeting one of those servers.
    * Remediation:
        * https://attack.mitre.org/techniques/T1171/ 
* Pupy
    * https://github.com/n1nj4sec/pupy 
* Mimikatz
    * https://adsecurity.org/?page_id=1821 
* EyeWitness
    * https://github.com/FortyNorthSecurity/EyeWitness 
* Sharpsploit
    * https://posts.specterops.io/introducing-sharpsploit-a-c-post-exploitation-library-5c7be5f16c51 
* EventLogParser
    * Detail: Parse the event logs for sensitive information.
    * Repo: https://github.com/djhohnstein/EventLogParser 
* BloudHoundAD
    * https://github.com/BloodHoundAD/BloodHound 
* Anubis
    * https://github.com/jonluca/Anubis 
* SharpUp
    * https://github.com/GhostPack/SharpUp
* CrackMapExec
    * https://github.com/byt3bl33d3r/CrackMapExec/wiki 
* Masscan
    * https://hml.io/2016/01/20/masscan-logging-demystified/ 
* Lists
    * https://github.com/infosecn1nja/Red-Teaming-Toolkit 
    * https://ngrok.com/ 
* Rumble Network Discovery Beta 5
    * https://rumble.run/2019/07/rumble-network-discovery-beta-5/ 
* SSHuttle
    * https://github.com/apenwarr/sshuttle 
    * Exfil/Infill over DNS.
* Maltego
* FOCA
* Creepy
* Atomic Red Team 
    * https://github.com/redcanaryco/atomic-red-team
* AMass
    * https://github.com/OWASP/Amass/blob/master/doc/user_guide.md 
* GoBuster
    * https://github.com/OJ/gobuster 
* Snallygaster
    * https://github.com/hannob/snallygaster 
* API Fuzz
    * https://github.com/smasterfree/api-fuzz
* Kerberos
    * https://github.com/GhostPack/Rubeus 
        * https://posts.specterops.io/kerberoasting-revisited-d434351bd4d1?gi=7659b3c5495a 
    * DeathStar: https://github.com/byt3bl33d3r/DeathStar 
* Printers
    * Pret: https://github.com/RUB-NDS/PRET 
* Firmware
    * firmwalker: https://github.com/craigz28/firmwalker

### Firewalls
* WAF Evasion: https://www.hahwul.com/2018/05/hacking-evasion-technique-using.html 

### Malware
* https://docs.microsoft.com/en-us/windows-hardware/drivers/debugger/getting-started-with-windbg 

### Hardening
* Kerberos: https://adsecurity.org/?p=3377 
    * https://ldapwiki.com/wiki/Security%20Support%20Provider 
* Active Directory
    * https://docs.microsoft.com/en-us/windows-server/identity/ad-ds/plan/security-best-practices/implementing-least-privilege-administrative-models 
    * https://docs.microsoft.com/en-us/windows-server/identity/ad-ds/plan/security-best-practices/appendix-f--securing-domain-admins-groups-in-active-directory 
    * https://www.beyondtrust.com/blog/entry/manage-windows-without-domain-admin-privileges 

### Blue Team
* https://github.com/mitre/caldera 

### Certificate Management
* https://github.com/smallstep/certificates 

### OSINT
* https://osintframework.com/ 
* https://www.internic.net/ 
    * Public Information Regarding Internet Domain Name Registration Services.
* https://osint.link/
    * OSINT tools and resources
* https://www.accuwebhosting.com/resources/who-is-hosting-this-website 
    * Checkout who is hosting this website. Discover which web hosting any web site uses, plus DNS records, whois information, server location and more.
* http://raidersec.blogspot.com/2012/12/automated-open-source-intelligence.html 
* Google Custom engine
    * Site examples
    * Pastbin.ca
    * Nopast.info
    * Paste.pocoo.org
    * Gramfeed.com
    * Twicsy.com
    * Scribd.com
    * Publicintelligence.net
    * Boardreader.com
    * Twitter.com
    * Sinodefence.com
    * Phibetaiota.net
    * Satp.org
    * Bellincat.com
    * 2lingual.com
    * Groups.google.com
    * Groups.yahoo.com
    * Yandex.com
    * Followyourworld.appspot.com
    * Wigle.net
    * Thatsthem.com
    * Pipl.com
    * Spokeo.com
    * Lullar.com
    * Mocavo.com
    * Classmates.com
    * Reddit.com
    * https://www.brbpublications.com/freeresources/Pubrecsites.aspx (government public records sites list)

### Cloud-ish
* Azure
    * https://github.com/azure/stormspotter 
* Containers
    * https://gist.github.com/FrankSpierings/5c79523ba693aaa38bc963083f48456c 

### General resources
* General resource links
    * https://rmusser.net/docs/Privilege%20Escalation%20&%20Post-Exploitation.html 
    * https://movaxbx.ru/2018/10/31/list-of-awesome-red-teaming-resources/
    * https://blog.redteam.pl/2019/05/badwpad-and-wpad-pl-wpadblocking-com.html  
    * https://hausec.com/ 
    * Cloud: https://github.com/appsecco/breaking-and-pwning-apps-and-servers-aws-azure-training/blob/master/documentation/SUMMARY.md#osint-against-cloud-targets 
    * https://tulpa-security.com/2017/07/18/288/
        * https://attackerkb.com/about 
        * Pivoting: https://artkond.com/2017/03/23/pivoting-guide/ 
    * https://wrongbaud.github.io/Holiday-Teardown/
    * https://electronics-notes.readthedocs.io/
    * https://blog.mbedded.ninja
* Training
    * http://opensecuritytraining.info
    * https://pentesteracademy.com
    * https://malwareunicorn.org/workshops/re101.html#0
    
### TODO
* Read about Service Principal Names (https://www.google.com/search?source=hp&ei=bEG1W9W9L6ft5gLNoYSoAw&q=service+principal+names&oq=service+prinipal+names&gs_l=psy-ab.3.0.0i13k1l2j0i13i30k1l6j0i13i5i30k1l2.229.3965.0.4416.25.18.1.0.0.0.281.2486.0j6j6.12.0....0...1c.1.64.psy-ab..12.13.2490.0..0j35i39k1j0i67k1j0i131k1j0i131i10k1j0i10k1.0.BIqI4zvtC3Y) 
* https://tpm.fail/ 
