# Linux Forensics
The notes found here are primarily from a Pentester Academy course (https://www.pentesteracademy.com/course?id=20) on Linux Foreinsics 

---
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Hardware](#Hardware)
- [Distro](#Distro)
- [Initial Methodology](#Initial-Methodology)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Hardware
* Write blockers

## Distro
* SIFT: https://digital-forensics.sans.org/community/downloads 

## Initial Methodology
* Mount drive
* Run a known good shell
* Set path to only point to your dirs
    * Export PATH=<path/to/mounted/safe/bin>
* Reset LD_LIBRARY_PATH
    * Export LD_LIBRARY_PATH=<path/to/mounted/safe/lib64
* Minimize disturbance to the system by either using netcat or store to usb drive to transport data.
* Netcat
    * Setup a listener with -k to keep alive and output to an outfile.*
    * Gather information such as the date, uname
* What to collect?
```bash
// Example showing the initial scan script. Other examples can be found in the other scripts provided. These will include files found in /var/log for example.
from the inital-scan.sh script
send-log.sh date 
send-log.sh uname -a
send-log.sh ifconfig -a
send-log.sh netstat -anp
send-log.sh lsof -V
send-log.sh ps -ef
send-log.sh netstat -rn
send-log.sh route
send-log.sh lsmod
send-log.sh df
send-log.sh mount
send-log.sh w
send-log.sh last 
send-log.sh cat /etc/passwd
send-log.sh cat /etc/shadow

// Example of diffing file systems to sort by modified date
find ./ -xdev -ls | awk '{print $8 $9 $10 $11}' > old.txt
find ./ -xdev -ls | awk '{print $8 $9 $10 $11}' > new.txt
diff old.txt new.txt
```


* Memory acquisition basics
    * Fmem: http://hysteria.sk/~niekt0/foriana/fmem_current.tgz
    * Old method
        * /dev/mem and /dev/kmem used to be accessible which was naturally a large security hole. This might be found on old systems.
        * If either /dev/mem or kmem are accessible on newer 32 bit systems then they should be limited to the first 869 MB of RAM.
    * fmem method:
        * Create a new device using fmem to operate similarly to the old method.
        * Use /proc/iomem to determine the appropriate bits.
        * Raw memory image is difficult to use for more simple searches.
        * Fmem will naturally make changes to the underlying system.
    * LiME method (preferred way)
        * Muse be built for the exact kernel
        * Should not be build on subject machine
        * Build from source:
            * Download and compile with correct headers.
            * Compile with “make” for current kernel or “make -C /lib/modules/<kernel version>/build M=$PWD” for other kernels.
        * Interesting portions
                * Cat /proc/iomem |grep “System RAM”
    * Last step before shutdown
        * Make sure to have a memory dump
         *Any last minutes scans
    * Normal shutdown
        * Filesystem should be clean
        * Malware might cleanup after itself and/or destroy evidence
    * Pulling the plug
        * Filesystem may not be clean
        * Could call sync before pulling the plug
    * No change for malware to destroy any info
    * Memory image already collected.
    * Creating an image
        * dd or dcfldd can be used. The latter will provide the raw data along with hashes.
            * dcfldd is specifically used for forensics. 
    * Write blocking
        * Hardware
            * Commercial for SATA only $350+
USB for cheap.
        * Software write blocking
            * Use udev rules
                * Udev is used for mounting devices.
            * Boot live forensics Linux on subject computer
            * Boot live forensics Linux on forensics workstation
            * Define: write blockers - https://www.cru-inc.com/data-protection-topics/write-blockers/ 
    * Creating an image from a physical drive
        * https://dfir.blog/imaging-using-dcfldd/

Questions
1. Why do we do live analysis before dead analysis? Shouldn’t we at least clone the drive before we do anything? There could be a time sensitive wipe or a wipe if forensic analysis is detected. 
Malware could cleanup during the shutdown process.
Need to grab what is in memory before shutting down.
2. I understand the modifying on the system that needs to be evaluated is a big no-no but what’s the point of LiME if we can’t evaluate the RAM of the target system? Is there a means of completing replicating a system, including what resides in RAM?
I think we have to compile on an identical image and then import the kernel module on the target system.
