This site is being used to capture notes taken from personal projects, courses, and general activities. Previously this content was being captured in private documents but I decided to instead share these notes publicly so that they could potentially be used to help others working in similar domains. The documentation found here is in a continual state of being a work in progress; expect to see unfinished sections, TODOs, and missing images. 

Site contents, along with other projects, can be found on Gitlab - https://gitlab.com/sigvis
