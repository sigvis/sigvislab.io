# Reverse Engineering

Notes include content from the following courses:
- Pentester Academy Course - https://www.pentesteracademy.com/course?id=40
- Hackaday Reverse Engineering with Ghidra - https://hackaday.io/course/172292-introduction-to-reverse-engineering-with-ghidra
---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Linux 32 Bit Applications](#linux-32-bit-applications)
- [Linux 64 Bit Applications](#linux-64-bit-applications)
<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Linux 32 Bit Applications

- Calling Conventions
  - Cdecl
    - Used primarily be C programs
    - All arguments are passed on the stack
    - Caller (not callee) must clean up the stack
    - Arguments pushed on stack from right to left
    - Methods that take a variable number of arguments (i.e. printf) must use this method.
  - Stddecl
    - Callee cleans up stack
    - Arguments pushed on stack from right to left
    - This method cannot be used with function that accept variable arguments
      - Callee cannot clean up the stack if there are a variable amount of arguments passed since the callee has no way of knowing what was passed.
    - Used by Windows
  - Fastcall
    - First two arguments passed in ECX, EDX registers
    - Callee cleans up stack if required
    - Additional arguments (if any) pushed on stack from right to left.
    - Note: There are multiple versions of Fastcall
  - Linux System Calls
    - System call number passed in EAX
    - Up to six arguments passed in EBX, ECX, EDX, ESI, EDI, EBP registers
    - Callee cleans up stack if required
    - If more than 6 arguments then the address is passed in EBX.
- ELF files
    - Components
        - Header
        - Sections
        - Segments
    - Symbols
        - Useful when debugging though they can be removed.
        - Examples
          - .symtab: Symbols used for debugging
          - .dynsym: Symbols used for dynamic linking
- Stack based Buffer Overflows
  - Successfully Exploiting Stack Buffer Overflows
    - Must overwrite return address on stack
    - Usually also must inject shellcode
  - The Stack
    - Overflow local variables
    - Stack grows downward
    - Overflowing variables will overwrite return address.
  - JMP ESP
    - When memory regions differ on where you’re trying to put your shellcode (ASLR) you can find a JMP ESP within executable memory space where you can put your shellcode. The JMP ESP (FF E4) command with executable memory space could be within the written program or within a loaded library. 
    - It should be noted that execution permissions on the stack might not be set. This would have to be a CLI arg when compiling the application.
  - Protections
    - ASLR
      - /pro/sys/kernel/randomize_va_space (set to 0 to turn off ASLR and back to 2 for original value)
    - Stack smashing
      - -fno-stack-protector (stack canary)
      - -z exestack (stack exec)
  - Return to Libc
    - A means of getting around the non-executable stack protections through the use of calling a function within libc. You can do this by finding the location of a function call such as system. 
    - Finding a function
      - readelf -s /lib32/libc-2.23.so |grep system 
        - Used to find all the functions, or just system, that are available in the library which we know was loaded during the binary’s execution. 
        - Example result: system@@GLIBC_2.0
      - After finding where it is in memory then you can load the executable, add the offset to the aforementioned memory location, and that will be the function (system) call. The parameters can either be passed on the stack given that they’re using cdecl or they could even be pulled out of an existing string within the application.
        - Partial strings can be pulled out. The example given was ‘monster bash’ where the location of ‘bash’ is referenced for popping a shell.
        - The address should not end in ‘00’ as it will cause issues with the payload.
        - When calculating the offset of the function being used make sure to add the location of the library address to the address where the function is at.
    - Fake framing
      - We will be doing the same process for return to libc except this process will include additional steps as chaining is required through the use of fake frames. This method can be useful when you encounter null bytes with a return to libc function call.
      - Apparently we’re supposed to be able to calculate an exit address but I’m not sure how we did this.
      - We will also need to find:
        - Address of function
        - Exit address
        - leave/ret address
        - Fake ebp addresses
  - ASLR and the PLT
    - ASLR Problem: The Library base address is unknown.
      - The offset into the library is still known
      - Solutions:
        - Use the Procedural Linkage Table (PLT)
        - Overwrite the Global Offset Table
        - Brute force (last resort)
    - PLT
      - Shared libraries are loaded into multiple programs at once.
        - Text segment is common to all programs.
        - Each program has its own private data segment.
        - Position Independent Code (PIC) in libraries
          - Relative positioning; not hardcoding addresses
    - Libraries functions are not called directly
      - A stub in the PLT is called; libraries are not called directly. The stub will use the dynamic linker to find the function within the library. The stub will then write to the GOT where that function resides.
      - Stub looks up address in the Global Offset Table (GOT)
      - PLT limitation would be that it requires that the program called the function that you’re trying to call with your shellcode since it will not be in the GOT unless it was called.
      - This technique would allow for an ASLR bypass and could be chained similarly to how the return to libc was chained.
  - GOT Overwrite and Dereference
    - The two ways to attack the GOT is to either overwrite it or dereferences (store offset in a register).
    - Locating GOT entries:
      - Objdump -R <program>
    - Methodology for the GOT overwrite
      - Find the offsets for a called function and desired function:
        - `readelf -s <library>`
      - Calculate the offset difference
        - Offset_delta = <desired address> - <called address>
      - Overwrite GOT
        - GOT[<called>] = GOT[<called>] + offset_delta
        - Once the overwrite has occurred then every time the original function is called then overwriting function will actually be called.
    - Methodology for the GOT dereference
      - Find offsets for a called function and desired function
        - `readelf -s <library>`
      - Calculate offset difference
        - Offset_delta = <desired addresses> - <called address>
      - Load address into register
        - EAX = GOT[<called>]
        - EAX = EAX + offset_delta
    - Return-Oriented Programming (ROP)
      - Building up the function you want
      - Make use of code immediately before a ret instruction
      - Code chunks before ret are called gadgets
      - Several tools to locate gadgets
        - Rp++
        - RopeMe
        - ROPgadget
        - objdump
          - This is for manually searching of course.
      - Build attack example (stack builds top to bottom so you have to go in the reverse order):
        - last gadget call
          - Buff += gadget3
          - Buff += gadget3_arg1
          - Buff += gadget3_arg2
        - 2nd to last gadget
          - Buff += gadget2
          - Buff += gadget2_arg1
          - Buff += gadget2_arg2
        - first gadget
          - Buff += gadget1
          - Buff += gadget1_arg1
  - Stack Canary
    - Value put on the stack
    - Not a return address or an argument
    - Use to prevent/detect stack modification
    - Extra code is added to function epilogues to check
    - Compiler inserts extra code
    - Two types in common use
      - Terminator
        - Contains characters that are incompatible with strcpy, etc
        - Example 0x000a0dff
          - Strcpy is blocked by null (0x00)
          - Gets and similar block by newline (0x0a and 0x0d)
          - Other functions interpret 0xff as End of File (EoF)
        - Static value can easily be searched on.
        - Attacking a terminator
          - Can overwrite the data up to the canary
          - Can still point to new location on the stack
          - Easiest if the stack is executable
            - Not strictly necessary though.
    - Random
      - Does stop string operators such as the terminator with strcpy
      - Only detects tampering
      - Much harder to thwart
      - Attacking a random canary
        - Extremely difficult in Linux
        - Somewhat easier in Windows
          - Use a Structured Exception Handler (SEH) exploit
          - Modification of stack raises an exception
          - If exception handler has been changed to shellcode → exploit
        - Brute forcing a random canary is not likely to be successful.
  - Summary
    - If the user has control of a variable passed into a function there may be an exploit
    - Overflowing this variable can overwrite the return address and thus redirect program execution
    - Several protections against exploitation
      - ALSR (by OS)
      - Not executable stack (compiler)
      - Stack canaries (compiler)
- Heap Buffer Overflows

## Linux 64 Bit Applications
- Registers
  - RIP
    - Points to the instruction which will be next executed.
    - 64 bits in width.
  - RFLAGS
    - Stores flags used for processor flow.
  - FPR0-FPR7
    - Floating point status and control registers.
  - RBP/RSP
    - Stack manipulation and usage.
- Instructions
  - push
    - will grow the stack by 8 and store the operand contents on the stack.
      - Example
        - push rax
          - Increases the value pointed to by rsp by 8, and stores rax there.
  - pop
    - will load the value pointed to be rsp into the operand.
      - Example
        - pop rbx
          - Loads the value pointed to by rsp into rbx, and decreases rsp by 8.
  - jmp
    - Change what code is being executed by modifying the value of EIP
  - call
    - used to implement function calls
  - cmp
    - performs a comparison by subtracting operands.
      - No storage is performed (unlike sub)
- Addressing Modes
  - Immediate
    - The value stored in the instruction
    - Example
      - add rax, 14; # Stores 14 into RAX
  - Register to Register
    - Example
      - xor rax, rax; # clears the value in RAX
  - Indirect Access
    - Examples
      - add rax, [rbx]; # adds the value pointed to by rbx into rax
      - mov rbx, 1234[8*rax+rcx]; move word at address 8*rax+rcx+1234 into rbx


- References
  - General
    - https://sensepost.com/blogstatic/2014/01/SensePost_crash_course_in_x86_assembly-.pdf 
    - http://phrack.org/issues/58/4.html
  - Stack Protections
    - https://mudongliang.github.io/2016/05/24/stack-protector.html 
    - https://security.stackexchange.com/questions/18556/how-do-aslr-and-dep-work 

------------------------------------------------


This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```yaml
# requiring the environment of NodeJS 8.9.x LTS (carbon)
image: node:8.9

# add 'node_modules' to cache for speeding up builds
cache:
  paths:
    - node_modules/ # Node modules and dependencies

before_script:
  - npm install gitbook-cli -g # install gitbook
  - gitbook fetch latest # fetch latest stable version
  - gitbook install # add any requested plugins in book.json
  #- gitbook fetch pre # fetch latest pre-release version
  #- gitbook fetch 2.6.7 # fetch specific version

# the 'pages' job will deploy and build your site to the 'public' path
pages:
  stage: deploy
  script:
    - gitbook build . public # build to public path
  artifacts:
    paths:
      - public
  only:
    - master # this job will affect only the 'master' branch
```

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] GitBook `npm install gitbook-cli -g`
1. Fetch GitBook's latest stable version `gitbook fetch latest`
1. Preview your project: `gitbook serve`
1. Add content
1. Generate the website: `gitbook build` (optional)
1. Push your changes to the master branch: `git push`

Read more at GitBook's [documentation][].

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

## Troubleshooting

1. CSS is missing! That means two things:

    Either that you have wrongly set up the CSS URL in your templates, or
    your static generator has a configuration option that needs to be explicitly
    set in order to serve static assets under a relative URL.

----

Forked from @virtuacreative

[ci]: https://about.gitlab.com/gitlab-ci/
[GitBook]: https://www.gitbook.com/
[host the book]: https://gitlab.com/pages/gitbook/tree/pages
[install]: http://toolchain.gitbook.com/setup.html
[documentation]: http://toolchain.gitbook.com
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
