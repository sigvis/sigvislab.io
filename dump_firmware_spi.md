# Trials, Tribulations, and Patience: Dumping Firmware over SPI
My intention in attempting to dump the firmware off of an embedded device was to practice my chip identification, target acquisition, and to work on dumping the contents of a chip directly. There are quite a few tutorials out there for dumping flash contents but few I’ve personally come across go through some of the troubleshooting aspects of that process. In security, I’ve found troubleshooting to be one of the most crucial skills to work on building; anyone can follow tutorials in ideal conditions. I’m writing this to share my experience in dumping firmware over SPI. 

_TL;DR:
The narrative found below goes into some detail around some of troubleshooting that occurred, and my reasoning in choosing the target I did. For those less interested in that process here are the steps I took to dump the firmware:_
1. Identify the target chip and pull down the datasheet (https://www.pjrc.com/store/w25q64fv.pdf)
2. Build flashrom (https://github.com/flashrom/flashrom) from source:
```bash
git clone https://github.com/flashrom/flashrom.git
cd <path/to/flashrom>
make && make install
```
3. Update the Bus Pirate to the latest community version.
4. Connect the Bus Pirate to the four SPI pins found on the target chip (CS --> CS, MISO -->  DO, CLK --> CLK, MOSI --> DI) while also connecting to the VCC and GND pins (3v3 --> VCC, GND --> GND) in order to power the chip.
5. Connect the Bus Pirate to the laptop and find the device with the following command:
```bash 
sudo dmesg
```
6. Run flashrom and output the contents to a bin file.
```bash 
sudo flashrom -p buspirate_spi:dev=<path/to/dev/USB*>,spispeed=1M -r <path/to/bin/file>
```
_Note: udev rules can be set so that non-root users can access the connected device and run flashrom. If you want to learn more about that method check out the Arch wiki - https://wiki.archlinux.org/index.php/Bus_pirate#udev_

<img src="img/modem_bp_hookup_p2_s.jpg"  width="360" height="480">


### Before we begin 
Personally I prefer to isolate my projects in a VM because of security reasons, and I find it easier to control the environment I’m working in so that I don’t have to worry about any software version/dependency conflicts. That way I don’t have to worry about cluttering my host machine with a bunch of software with varying versions. As we’ll see later though that VM becomes an issue. For those wanting to replicate my host environment I’ve provided some OS, firmware, and software details at the end of this post.

### Why are we still here? Just to suffer?
Initially my target was a TP-Link TL-WR841N, which I’ll get into, but as we’ll see I was not able to successfully dump the firmware of this device over SPI. There are UART pads present on the board but I wanted to stick to dumping firmware directly from a chip instead as my goal was to gain more experience in interfacing directly with a chip.
The TP-Link TL-WR841N is a SOHO router that can be found on Amazon for $20 (https://www.amazon.com/gp/product/B001FWYGJS/ref=as_li_tl?ie=UTF8&tag=bntl-wr841n-20&camp=1789&creative=9325&linkCode=as2&creativeASIN=B001FWYGJS&linkId=832d0ab38990d52fbe69244e5df26384) as of writing this post. The board presents an EEPROM flash chip that uses SPI for inter-chip communication (http://www.elm-tech.com/en/products/spi-flash-memory/gd25q32/gd25q32.pdf). To attempt the dump I decided on using a few tools: a Bus Pirate v3.6 (http://dangerousprototypes.com/docs/Bus_Pirate) and flashrom (https://github.com/flashrom/flashrom). The version of the Bus Pirate firmware was 5.10 and 4.4 for the bootloader. Despite the old version of firmware on the Bus Pirate, I was going to attempt the dump. Before dumping though I decided to build flashrom from source to get the latest version. 
```bash
git clone https://github.com/flashrom/flashrom.git
cd <path/to/flashrom>
make && make install
```
The first few runs resulted in a few different results: flashrom hanging during the read or flashrom not detecting EEPROM/flash.

![alt text](img/eepromNotFound.jpg "EEPROM/flash not found")

The first issue could be occasionally resolved by manually specifying the chip but even that would sometimes result in no flash device being found. That latter can be resolved by running flashrom outside of the VM and updating the firmware of the Bus Pirate. I do not currently have a definitive answer as to why it’s not working within the VM but it could be due to VirtualBox’s USB implementation with my current version or even just a driver related issue. Do note that I have installed VirtualBox’s extension pack to support USB.
After modifying my methodology of dumping the flash contents from my host machine, instead of using the VM, I was able to get flashrom to write out to a binary. There was an issue though… There did not appear to be any content present within flash.

![alt text1](img/router_zeroed.png "0x00")

Troubleshooting consisted of attempting multiple runs in different environments and even included me purchasing another TP-Link TL-WR841N to verify that the issue wasn’t isolated to one device.

### Less tribulations; more profit
The next step I decided to take was to find another target to help narrow down the possible issues. Dumping the firmware off another device would increase the likelihood that my methodology is correct though the chip that I’m targeting might not be. Not dumping the contents of the alternative target would have led me down the path of reevaluating how I’m dumping. The alternative target would be an old modem I had from years ago when it was required by my ISP. I was hoping to find another device that supported SPI to see if either my setup or actions were causing issues with the dump from the TP-Link, or if the target itself might not be behaving in accordance with my expectations.
After taking the &lt;redacted&gt; modem apart we can see that it contains a SLHCY DNCE2530G processor, a Winbond W9725G6KB (https://datasheetspdf.com/pdf-file/988768/Winbond/W9725G6KB/1), 54231 AST1 (https://pdf1.alldatasheet.com/datasheet-pdf/view/250065/TI/TPS54231.html), 88E1119R-NNW2 Marvell IC - the chip for the RJ45 port (https://www.datasheetarchive.com/pdf/download.php?id=95922b3c610b498e678431554f60b6c7e45a73&type=P&term=88E1119R-NNW2), and a Winbond 25Q64FVF1G to name a few chips on the board. The chip on the back of the PCB, Winbond 25Q64FVF1G, looked to be the most promising given the datasheet (https://www.pjrc.com/store/w25q64fv.pdf) describes it as flash memory. As we can see from the datasheet this flash chip is leveraging a 16 pin chip, instead of the previous 8 pin chip seen on the router, as the chosen chip contains multiple No Connect pins. For our purposes though we will be using the standard SPI setup with four pins: data out (DO) and data in (DI) for the data lines, one for clock (CS), and the other for the chip select (CS). An additional two pins will also be used to power and ground the device. Those pins are the voltage common collector (VCC) and ground (GND) respectively.The Bus Pirate pins that are used for connecting use a different naming scheme than what is seen on the datasheet due to it being a programmer. The data pins on the Bus Pirate are known as the Master Out Slave In (MOSI) and Master In Slave Out (MISO), and the 3v3 to denote the 3.3 volts. In order to tell which pin is which on the target chip look for a small circle indentation mark in one of the corners. In the image below, that circle can be slightly made out in the top left corner. Once I connected the Bus Pirate to the modem (CS --> CS, MISO -->  DO, CLK --> CLK, MOSI --> DI, 3v3 --> VCC, GND --> GND), and used the method of dumping flash outside the VM with the updated Bus Pirate firmware, I was able to complete a dump of the firmware without issue.

<img src="img/modem_bp_hookup_s.jpg"  width="360" height="480">

There are a few things that can be achieved once the firmware is dumped but the next steps that I’ll be taking in my process will entail a bit of reversing to assess what is present within the dumped contents; possibly sensitive information or even vulnerabilities.
There are quite a few ways to protect the unencrypted firmware from being extracted for reversing with options that range from additional hardware on the board to software based remediation. One of the more secure options would be a discrete TPM where a dedicated tamper resistant circuit is mounted to the board but incorporating an entirely new circuit to an already deployed modem would require a hardware redesign; possibly unnecessarily expensive for an old modem. The firmware TPM (fTPM) or Intel’s Platform Trust Technology (PTT) is also another potential avenue but would require support for the Intel CPU that is present on the board. I have yet to find evidence on https://ark.intel.com on whether or not there is the potential for fTPM/PTT support given the chip but that is for another day of digging!

### Acknowledgements
Below I’ve listed some of the wonderful resources which helped me through this project but there are two specifically I would like to mention. Both Tim Michaud (https://twitter.com/TimGMichaud) and Veer Singh (https://twitter.com/daymanbyday) helped me review this write-up prior to publication. Tim has some great content on his blog (https://www.inulledmyself.com/) ranging from helping people break into the security community to PHP 0Days. Make sure to check out Veer’s write-up on deconstructing USB as well! - https://www.linkedin.com/pulse/deconstructing-iot-series-understanding-how-usb-protocol-veer-singh/

##An aside
To those that are new to the embedded space (or the technology space in general): Don’t feel too dejected when what you think should be easy takes time. I spent probably 20 hours over the course of two weeks troubleshooting issues related to the dump but from the time I chose the alternative target of &lt;redacted target&gt;, took it apart, found a datasheet, connected to the chip, and dumped the contents the whole process took only about 10 minutes. __Welcome to tech.__

### Technology stack
* Host machine operating system: Devuan (a Debian fork) (https://www.devuan.org/)
* Bus Pirate v3.6: http://dangerousprototypes.com/docs/Bus_Pirate
* Bus Pirate bootloader: v4.4
* Bus Pirate firmware: Community version 7.1 (https://github.com/BusPirate/Bus_Pirate) 
* Flashrom: v1.2-116-gc64486b
* Target device: &lt;redacted&gt;

### References
* https://www.analog.com/en/analog-dialogue/articles/introduction-to-spi-interface.html 
* https://jcjc-dev.com/2016/06/08/reversing-huawei-4-dumping-flash/ 
* https://gracefulsecurity.com/extracting-flash-memory-over-spi/ 
* https://electronics.stackexchange.com/questions/194693/why-do-ics-have-nc-no-connection-or-no-function-pins/194694 
* https://www.avrfreaks.net/forum/misomosi-vs-dido-spi-communication
* https://docs.microsoft.com/en-us/windows/security/information-protection/tpm/tpm-recommendations
* https://www.kernel.org/doc/html/latest/security/tpm/tpm_ftpm_tee.html
* https://www.microsoft.com/en-us/research/wp-content/uploads/2017/06/ftpm1.pdf

