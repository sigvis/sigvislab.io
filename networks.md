# Networks
General network related notes

---
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Cisco Pluralsight Course](#cisco-pluralsight-course)
- [Preparation Guides](#preparation-guides)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Cisco Pluralsight Course 
These notes were from the following Pluralsight course - https://app.pluralsight.com/library/courses/networking-cisco-ccna-200-125-100-105
- IPv4
  - Subnets
    - Partitioning
      - Figure out how many host addresses will be needed. Then round up for what is permitted by subnet. 
      - If we are given a large network portion assigned by an ISP, and told that we need X amount of networks, then we can determine the subnet mask by going to the largest permitted number of networks through some division. The division occurs within a ‘borrowed bits’ section which we end up pulling from available host portion to account for a need for X amount of networks. Don’t overthink this. This is a very simple delineation. 
    - Broadcast address
      - The last address in the subnet is reserved as a broadcast address. This is often seen as a x.255
    - Host address
      - The first address in the host portion is reserved as the host address and will often be denoted with a x.0
    - Subnet mask	
      - The purpose of having a subnet mask is to differentiate which part of an address is the network portion and the host portion. The origination of subnet mask was to account for the limited form permitted with the IPv4 range.
- IPv6
  - Structure
    - Network portion: Tends to be at 64 bits long
    - Interface identifier (host): Tends to be 64 bits long
    - Leading 0s in each hextet get removed.
    - Double colon can be used to remove adjacent hextets that only contain 0s. Two double colons can not be used within an IPv6 address because the address will not be able to be reversed.
    - Unicast: 
      - Global address: Used for global communication (internet facing address)
      - Link local address: Used for LAN communications. It will always be prefixed with an FE80.
      - Loopback address: Will be denoted with a ::1/128
      - Unspecified address: ::/128
      - Unique Local: FC00:/7 will be used when no communications want to be sent to other devices.
    - Multicast: 1 to many
    - Anycast address: Used for load balancing
    - SLAAC: Stateless Address Auto Address-configuration
      - Windows: Random interface identifier is provided
      - Unix/Linux/Mac: Modified EUI (this in clearly insecure)
        - Mac Address will be split into two pieces (24/24) and ‘FF:EE’ will fill in between the split. It will then convert the hex to binary, flip the 7th bit, and then convert it back to hex.
        - Neighbor advertisement will be sent to say ‘howdy, I have this address’
        - Use DHCP instead.
    - DHCP
      - Use this instead of SLAAC. This will operate similarly to IPv4 and will assign an IPv6 address.
  - Subnet
    - Free IPv6 Public Space: https://tunnelbroker.net/ 
    - Subnetting is broken down in a similar way, but instead of breaking down a subnet by incrementing by 1, we will instead increment by 4 because there are four bits in one hexadecimal character.
- Router Operation
  - Ping example:
    - End user wants to send a ping message to a server on another private network. 
    - The network gateway for each network has been configured so that both the server and end user know how to reach one another.
    - The ping message gets encapsulated within a TCP/IP packet which then is encapsulated within a frame. The packet will contain the src and dst ip address.
    - The frame will have the source MAC address of the sender and the dst MAC address of the router gateway. The frame will be removed once it reaches the router gateway. A new frame will be generated when it exists the router with the src being the router gateway and the dst MAC address being the server.
    - The process will reverse after it removes the frame, then the packet. It will process the ping message and ultimately reply to the ping message.
- Variable Length Subnet Masks (VLSM)
  - A different method of calculating submasks in a more efficient way.




TODO: Insert pictures




  - Make sure to start with the largest amount of needed hosts and descend from there when generating the table.
- Cabling
  - Crossover cable: Used to connect a PC to a router for ethernet.
  - Configuration cables
    - Rollover cable: Used to interface with a router when nothing has been configured. PC (RS232 → rollover cable → console port
- Router Configuration
  - Modes of operation: Usermode, privileged mode, and configuration mode
  - Commands

``` bash
      Privileged mode: enable
      User mode: disable
      Configuration mode: configure terminal
      Hostname <name> // needed to configure ssh
      ip domain-name <name>
      Banner motd #<text>#
      //Configuring password for console 0
        line console 0
        password <password>
        login // makes it so that auth is enabled.
      //Configure password for privileged mode
        Configure terminal
        Enable secret <password>

      //Prevent annoying (async) logging
        Logging sync
      //Configure ssh
        config t
        crypto key generate rsa general-keys
        ip ssh version 2
        username <name> secret <password>
        line vty 0  4 // allows for 5 ssh connections simultaneously
        transport input ssh // only allow ssh
        login local // user the local DB to auth users
        logging sync
        exit

      //Configuring interfaces (IPv4)
        interface fastethernet 0/0
        ip address <internal-ip> <mask>
        shutdown // take interface offline
        no shutdown // turn interface on

      //Save router state
        copy running-config startup-config

      //Erase router state
        Show startup-config // verify config exists
        Erase startup-config

      //Factory default configuration
        // after erased startup
        Reload // out of the box config. Don’t enter the initial configuration dialog

      //Configure IPv6 routing
        Ipv6 unicast-routing
        Int f0/0
        Ipv6 address <ipv6-address>
        copy run start

      //Upgrading an IOS router
        show version // show the current firmware in use
        show flash // need to check space is available
        delete flash:/<path-to-current-bin> // if there is not enough space to host the firmware which will be used for upgrading.
        copy tftp  flash [ENTER]
          <src-ip> [ENTER]
          <src-filename> [ENTER]
          <dst-filename> [ENTER]
        show flash // verify that the file was transferred
        config t
        boot system flash:/<filename>
        copy run start
        reload
        show version // verify the new IOS version is being used.

      //Authentication bypass to set new password
        // need access to the console port and router on/off switch
        // connect with screen/putty
        // issue a break command after the start (POST + bootstrap has been loaded)
        // now you will be in rom monitor mode
        confreg 0x2142 (default is 0x2102)
        reset
        enable
        show startup-config // see the startup config that was ignored due to confreg
        copy startup-config running-config
        config t
        enable secret <password>
        config-register 0x2102
        Int f0/0
        no shut
        exit
        copy run start
        whow ip interface brief // check to make sure the interfaces are up
        reboot
```


- Reformat (to contain nothing; no IOS)
  - Format flash
- Useful show commands

```bash
show running-config
show version
show flash
show ip interface brief
(in config mode) do show interface brief
show interface f0/0
(in config mode) no ip domain-lookup // removes the default of attempting to connect to some telnet server by default.
```

- PHY Ethernet
  - Cabling
    - Twisted pair cabling
      - Pretty standard and is what it sounds when cut open.



    - Crossover cables are used when we need to connect two of the same devices to each other such as a router/router, switch/switch, or computer/computer. Modern NICs automatically figure out the pinouts without the need for a crossover cable. Other configurations when needing include a computer/router.
    - Coaxial cabling


    - Serial cabling
    - Proprietary cabling
    - Fiber
      - Multi-mode
        - Lower (100m) than single mode, but less efficient. It’s cheaper than single mode due to the quality of the setup.
      - Single mode
        - Long distances
  - Wireless Ethernet
    - 2.4 GHz has 14 channels. Channels overlap so APs are put on channel 1,  6, or 11.
    - 5 GHz has more channels, but many of them require DFS to use so that they can hop off if a military application is detected on that channel.
- Data Link Layer
  - Ethernet
    - One of the layer 2 techs
  - Wireless
    - Beamforming is cool.
    - Satellite
      - ~35K miles away which will induces a .25 second delay.
  - Internet access
  - Serial links - PPP



- Ethernet
  - CSMA/CD (Carrier sense multiple access/collision detection)
    - Think of a bus for networking where multiple machines are connected to a bus and wait for there to be no traffic on the bus before they send. While this visual does not account for modern implementations, it serves as a baseline to build from.
    - An example could be a shared phone line where there are multiple phones in the same house, but they all share one line. A dial tone would indicate that no one would be using it. 
  - Duplex and Speed
  - Frame
    - Components
      - Destination
      - Source MAC address
        - Type (layer 3 protocol used within the data segment)
          - IPv4: 0x0800
          - IPv6: 0x8600
          - ARP: 0x0806
        - Data segment (1500 byte max)
        - FCS (Frame Check Sequence)
          - 32 bit CRC


- Ethernet Switching
  - Topologies
    - Bus
      - Shared wire. 5 volts will tell others on the same bus that the bus is current in use. Collision will happen if two devices are talking at the same time. Collision detection is possible because two 5volts will result in 10 volts on the wire. Not typically used.
    - Ring
      - Not typically used.
    - Star
      - Pretty much what we will always see. Each workstation will get a separate wire that will connect to a central device. That hub/switch will will repeat that message to all other devices on the network.
  - MAC Address Table
    - Switch vs hub
      - Traditionally the primary difference between a switch and a hub is that a switch will build a MAC Address Table and only send information from one device to the intended device instead of sending the message to all devices. That is permitted after a “circuit” is built, and due to this efficiency the switch can now support multiple devices sending multiple messages at the same time.
    - Flooding occurs when a MAC address is not in the MAC address table. The difference between a flood and a broadcast is that the broadcast has a specific address (FFFF:FFFF:FFFF) and has a specific purpose which will be understood by all clients. A flood frame will be ignored by clients if they do not know how to route the frame.
  - Broadcasts and Broadcast Domains
    - A switch will breakup a collision domain and a router will breakup a broadcast domain.
    - Broadcast domain is a group of devices which will receive layer 2 broadcast messages.
- Switch Configuration
  - Memory/Storage
    - Bootstrap is stored in EEPROM
    - IOS is stored in Flash
    - Startup-config (“config.text” on a switch) is stored in NVRAM (virtual NVRAM on a switch)
    - Running-config is stored in RAM
  - Side notes
    - A traditional switch operates as a layer 2 device so it will not handle any IP addresses or packet inspection. An ip address can be assigned to a switch though if you want to configure it to use ssh for remote login. Layer 3 switches do exist, but we have not covered them yet.
- Terminology
  - Bit: 1 or 0
  - Nibble: 4 bits
  - Byte: 8 bits
  - Hextet (Word): 16 bits
  - IPv4: 4 octets (32 bits)
  - IPv6: 8 hextets (128 bits)
- Hardware
  - USB to serial adapter: Tripplite Keyspan USA-19HS

## Preparation guides
- Study guides
  - Todd Lammle - book
  - Cisco Networking Academy
- Labs
  - GNS3/VIRL (virtualized environments)
- Lab Equipment
  - Cisco 1841 Routers
  - Cisco Catalyst 2960 Switches
  - Cisco Catalyst 3560 Switch


----

Forked from @virtuacreative

[ci]: https://about.gitlab.com/gitlab-ci/
[GitBook]: https://www.gitbook.com/
[host the book]: https://gitlab.com/pages/gitbook/tree/pages
[install]: http://toolchain.gitbook.com/setup.html
[documentation]: http://toolchain.gitbook.com
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
